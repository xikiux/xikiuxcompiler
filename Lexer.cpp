#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>

#include "Error/ErrorHandler.cpp"
#include "SymbolTracker.cpp"
#include "NecessaryLibraryDetector.cpp"

#include "Serializers/JSONGenerator.cpp"

namespace Lexing {
	enum TokenType{
		TTWhiteSpace, // 0
		
		TTFunctionDec, // 1
		TTMain, // 2
		TTLongDec, // 3
		TTOpenArgs, // 4
		TTCloseArgs, // 5
		TTOpenFuncBody, // 6
		TTCloseFuncBody, // 7
		TTReturn, // 8
		TTLongLiteral, // 9
		TTEndStatement, // 10
		TTPrint, // 11

		TTIdentifier, // 12
		TTAssignment, // 13

		TTPlusEquals, // 14
		TTMinusEquals, // 15
		TTTimesEquals, // 16
		TTDivideByEquals, // 17
		TTModEquals // 18
	};

	typedef struct Token {
		TokenType type;
		void* data;

		Token(TokenType _type, void* _data) {
			type = _type;
			data = _data;
		}
	} Token;

	typedef std::tuple<TokenType, int> Precedence;

	class Precedencer {
		std::vector<Precedence> precedence;
	public:
		Precedencer() {
			precedence = std::vector<Precedence>();

			precedence.push_back({ TTFunctionDec, 0 });
			precedence.push_back({ TTMain, 0 });
			precedence.push_back({ TTLongDec, 0 });
			precedence.push_back({ TTOpenArgs, 0 });
			precedence.push_back({ TTCloseArgs, 0 });
			precedence.push_back({ TTOpenFuncBody, 0 });
			precedence.push_back({ TTCloseFuncBody, 0 });
			precedence.push_back({ TTReturn, 0 });
			precedence.push_back({ TTPrint, 0 });
			precedence.push_back({ TTLongLiteral, 0 });
			precedence.push_back({ TTEndStatement, 0 });
			precedence.push_back({ TTAssignment, 0 });
			precedence.push_back({ TTPlusEquals, 0 });
			precedence.push_back({ TTMinusEquals, 0 });
			precedence.push_back({ TTTimesEquals, 0 });
			precedence.push_back({ TTDivideByEquals, 0 });
			precedence.push_back({ TTModEquals, 0 });
			
			precedence.push_back({ TTIdentifier, 1 });
		}

		Token* GetHighestPrecedenceToken(std::vector<Token*> foundTokens){
			Token* temp = foundTokens[0];

			for (int i = 0; i < foundTokens.size(); i++) {
				if (precedence[foundTokens[i]->type] < precedence[temp->type]) {
					temp = foundTokens[i];
				}
			}

			return temp;
		}
	};

	class Lexer{
		ErrorHandling::ErrorHandler* err;
		SymbolTracking::SymbolTable* sym;
	public:
		std::vector<Token*>* tokens;

		Lexer(ErrorHandling::ErrorHandler* errorHandler, SymbolTracking::SymbolTable* symbolTable){
			err = errorHandler;
			sym = symbolTable;
		}

		void Lex(std::string* program){
			tokens = new std::vector<Token*>();

			int currentIndex = 0;

			while (currentIndex < program->length()) {
				std::vector<Token*> foundTokens = std::vector<Token*>();
				int specialIncrement = 0;

				Token* tempToken;

				// get each token
				// whitespace
				if ((*program)[currentIndex] == ' ' || (*program)[currentIndex] == '\r' || (*program)[currentIndex] == '\n' || (*program)[currentIndex] == '\t') {
					currentIndex++;
					continue;
				}
				if (isStringThere(program, currentIndex, "function")) {
					foundTokens.push_back(new Token(TTFunctionDec, (void*)"function"));
				}
				if (isStringThere(program, currentIndex, "main")) {
					foundTokens.push_back(new Token(TTMain, (void*)"main"));
				}
				if (isStringThere(program, currentIndex, "long")) {
					foundTokens.push_back(new Token(TTLongDec, (void*)"long"));
				}
				if (isStringThere(program, currentIndex, "(")) {
					foundTokens.push_back(new Token(TTOpenArgs, (void*)"("));
				}
				if (isStringThere(program, currentIndex, ")")) {
					foundTokens.push_back(new Token(TTCloseArgs, (void*)")"));
				}
				if (isStringThere(program, currentIndex, "{")) {
					foundTokens.push_back(new Token(TTOpenFuncBody, (void*)"{"));
				}
				if (isStringThere(program, currentIndex, "}")) {
					foundTokens.push_back(new Token(TTCloseFuncBody, (void*)"}"));
				}
				if (isStringThere(program, currentIndex, "return")) {
					foundTokens.push_back(new Token(TTReturn, (void*)"return"));
				}
				if (isStringThere(program, currentIndex, "=")) {
					foundTokens.push_back(new Token(TTAssignment, (void*)"="));
				}
				if (isStringThere(program, currentIndex, ";")) {
					foundTokens.push_back(new Token(TTEndStatement, (void*)";"));
				}
				if (isStringThere(program, currentIndex, "print")) {
					foundTokens.push_back(new Token(TTPrint, (void*)"print"));
				}
				if (isStringThere(program, currentIndex, "+=")) {
					foundTokens.push_back(new Token(TTPlusEquals, (void*)"+="));
				}
				if (isStringThere(program, currentIndex, "-=")) {
					foundTokens.push_back(new Token(TTMinusEquals, (void*)"-="));
				}
				if (isStringThere(program, currentIndex, "*=")) {
					foundTokens.push_back(new Token(TTTimesEquals, (void*)"*="));
				}
				if (isStringThere(program, currentIndex, "/=")) {
					foundTokens.push_back(new Token(TTDivideByEquals, (void*)"/="));
				}
				if (isStringThere(program, currentIndex, "%=")) {
					foundTokens.push_back(new Token(TTModEquals, (void*)"%="));
				}
				long long tempLongLitStringLength = getLongLiteralLength(program, currentIndex); if (tempLongLitStringLength > 0) {
					foundTokens.push_back(new Token(TTLongLiteral, (void*)std::stoll(program->substr(currentIndex, tempLongLitStringLength))));
					specialIncrement = tempLongLitStringLength;
				}
				std::string* tempIdentifier = new std::string(getIdentifier(program, currentIndex)); if (*tempIdentifier != "") {
					foundTokens.push_back(new Token(TTIdentifier, (void*)tempIdentifier));
					specialIncrement = tempIdentifier->length();
				}

				// filter tokens
				if (foundTokens.size() < 1) {
					err->ThrowError(ErrorHandling::ErrorType::ETNoTokenFound, "In tokenizer, no token was found. Data is the index of failure.", "long long", (void*)(long long)currentIndex);
				} else {
					tempToken = (*new Precedencer()).GetHighestPrecedenceToken(foundTokens);

					tokens->push_back(tempToken);

					switch (tempToken->type) {
						case TTLongLiteral:
						case TTIdentifier:
							currentIndex += specialIncrement;
							break;
						case TTFunctionDec:
							currentIndex += 8;
							break;
						case TTReturn:
							currentIndex += 6;
							break;
						case TTPrint:
							currentIndex += 5;
							break;
						case TTMain:
						case TTLongDec:
							currentIndex += 4;
							break;
						case TTPlusEquals:
						case TTMinusEquals:
						case TTTimesEquals:
						case TTDivideByEquals:
						case TTModEquals:
							currentIndex += 2;
							break;
						case TTOpenArgs:
						case TTCloseArgs:
						case TTOpenFuncBody:
						case TTCloseFuncBody:
						case TTEndStatement:
						case TTAssignment:
							currentIndex++;
							break;
						default:
							err->ThrowError(ErrorHandling::ErrorType::ETNoTokenFound, "In the token increment table, no token was matched.", "TokenType", (void*)tempToken->type);
					};
				}
			}
		}

	private:
		bool isStringThere(std::string* program, int index, std::string keyWord){
			if (index + keyWord.length() - 1 < program->length()){
				if (program->substr(index, keyWord.length()).compare(keyWord) == 0) {
					return true;
				}
			}

			return false;
		}

		long long getLongLiteralLength(std::string* program, int index){
			char validChars[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

			int lastCorrectIndex = 0;

			bool longLitFound = false;
			bool digitFound = true;
			bool negativeFound = false;

			if (program->operator[](index) == '-') {
				lastCorrectIndex++;

				for (int i = 1; digitFound; i++) {
					digitFound = false;

					for (int j = 0; j < 10; j++) {
						if (validChars[j] == (*program)[index + i]) {
							lastCorrectIndex++;
							longLitFound = true;
							digitFound = true;
							break;
						}
					}
				}
			} else {
				for (int i = 0; digitFound; i++) {
					digitFound = false;

					for (int j = 0; j < 10; j++) {
						if (validChars[j] == (*program)[index + i]) {
							lastCorrectIndex++;
							longLitFound = true;
							digitFound = true;
							break;
						}
					}
				}
			}

			if (!longLitFound) { return 0; }

			return lastCorrectIndex;
		}

		unsigned long long getUnsignedLongLiteralLength(std::string* program, int index){
			char validChars[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

			int lastCorrectIndex = 0;

			bool longLitFound = false;
			bool digitFound = true;

			for (int i = 0; digitFound; i++) {
				digitFound = false;

				for (int j = 0; j < 10; j++) {
					if (validChars[j] == (*program)[index + i]) {
						lastCorrectIndex++;
						longLitFound = true;
						digitFound = true;
						break;
					}
				}
			}

			if (!longLitFound) { return 0; }

			return lastCorrectIndex;
		}

		std::string getIdentifier(std::string* program, int index) {
			char validChars[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
								 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
								 '_' };

			bool identifierFound = false;
			bool charFound;

			int length = 0;

			do {
				charFound = false;

				for (int chara = 0; chara < 53; chara++) {
					if (validChars[chara] == (*program)[index + length]) {
						charFound = true;
						identifierFound = true;
						break;
					}
				}

				if (!charFound) { break; }

				length++;
			} while (charFound && index + length < (*program).length());

			if (length > 0) {
				return (*program).substr(index, length);
			} else {
				return "";
			}
		}
	public:
		std::string GenerateJSON(){
			JSONGenerating::JSONGenerator jsonGen = JSONGenerating::JSONGenerator();
			
			jsonGen.MakeOpen("{");
			jsonGen.MakeLine("tokens", "[");

			for (int i = 0; i < tokens->size(); i++){
				jsonGen.MakeOpen("{");
				jsonGen.MakeVariable("type", JSONTypeIntToString((*tokens)[i]->type), JSONGenerating::JSONVarType::JVTString, true);
				if ((*tokens)[i]->type != TTLongLiteral) {
					if ((*tokens)[i]->type == TTIdentifier) {
						jsonGen.MakeVariable("data", (*(std::string*)((*tokens)[i]->data)), JSONGenerating::JSONVarType::JVTString, false);
					} else {
						jsonGen.MakeVariable("data", std::string((char*)((*tokens)[i]->data)), JSONGenerating::JSONVarType::JVTString, false);
					}
				} else {
					jsonGen.MakeVariable("data", std::to_string((long long)(*tokens)[i]->data), JSONGenerating::JSONVarType::JVTNumber, false);
				}
				jsonGen.MakeClose("}", (i != tokens->size() - 1) ? true : false);
			}

			jsonGen.MakeClose("]", false);
			jsonGen.MakeClose("}", false);

			return jsonGen.JSON;
		}
	private:
		std::string JSONTypeIntToString(TokenType type){
			switch (type) {
				case TTFunctionDec:
					return "TTFunctionDec";
				case TTMain:
					return "TTMain";
				case TTLongDec:
					return "TTLongDec";
				case TTOpenArgs:
					return "TTOpenArgs";
				case TTCloseArgs:
					return "TTCloseArgs";
				case TTOpenFuncBody:
					return "TTOpenFuncBody";
				case TTCloseFuncBody:
					return "TTCloseFuncBody";
				case TTReturn:
					return "TTReturn";
				case TTLongLiteral:
					return "TTLongLiteral";
				case TTEndStatement:
					return "TTEndStatement";
				case TTIdentifier:
					return "TTIdentifier";
				case TTAssignment:
					return "TTAssignment";
				case TTPrint:
					return "TTPrint";
				case TTPlusEquals:
					return "TTPlusEquals";
				case TTMinusEquals:
					return "TTMinusEquals";
				case TTTimesEquals:
					return "TTTimesEquals";
				case TTDivideByEquals:
					return "TTDivideByEquals";
				case TTModEquals:
					return "TTModEquals";
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(type), "null", 0);
			}
		}
	};
}
