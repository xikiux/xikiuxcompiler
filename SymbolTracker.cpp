#pragma once

#include <string>
#include <map>

namespace SymbolTracking {
    enum SymbolType {
        STUnknown,
        STFunctionName,
        STVariableName
    };

    class SymbolTable {
    public:
        std::map<std::string, SymbolType> symbols;

        SymbolTable() {
            symbols = std::map<std::string, SymbolType>();
        }

        void AddUnknownSymbol(std::string name) {
            symbols.insert(std::pair<std::string, SymbolType>(name, SymbolType::STUnknown));
        }

        void AddFunction(std::string name) {
            symbols.insert(std::pair<std::string, SymbolType>(name, SymbolType::STFunctionName));
        }

        void AddVariable(std::string name) {
            symbols.insert(std::pair<std::string, SymbolType>(name, SymbolType::STVariableName));
        }

        bool ContainsSymbol(std::string name) {
            if (symbols.find(name) != symbols.end()) {
                return true;
            }

            return false;
        }
    };
}