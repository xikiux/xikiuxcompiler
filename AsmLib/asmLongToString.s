_LTS:
	; function prologue
	push rbp
	mov rbp, rsp
	; save registers
	push r8 ; save r8
	push r9 ; save r9
	push r10 ; save r10
	; get parameters
	mov r8, [rbp + 16] ; get parameter
	mov r9, LTSnum ; set index to LTSnum
	mov r10, 20 ; length for skipping extra zeros
	; set up digit writing
	add r9, 19 ; adjust index to end of string
_LTSCheckZero:
	cmp r8, 0
	jne _LTSNextDigit
	; write zero
	mov [r9], byte 48 ; add digit to string
	sub r10, 1 ; decrement zero skip length
	jmp _LTSEnd
_LTSNextDigit:
	; check if still converting number
	cmp r8, 0 ; while number is still being converted
	je _LTSEnd
	; do division to get digit
	mov rax, r8 ; push parameter for dividing
	xor rdx, rdx ; clear rdx
	mov rcx, 10 ; set up divisor
	div rcx ; do division
	; write digit
	add dl, 48; adjust number to readable digit
	mov [r9], dl ; add digit to string
	sub r9, 1 ; subtract i by one
	mov r8, rax; move new total to r8 !!!
	sub r10, 1 ; decrement zero skip length
	jmp _LTSNextDigit
	; check if number is equal to zero
_LTSEnd:
	; print number
	mov rax, 1
	mov rdi, 1
	mov rsi, LTSnum
	add rsi, r10 ; skipping zeros
	mov rdx, 21
	syscall
	; restore registers
	pop r10
	pop r9
	pop r8
	; function epilogue
	mov rsp, rbp
	pop rbp
	ret

section .data
	LTSnum: db "00000000000000000000", 10, 0