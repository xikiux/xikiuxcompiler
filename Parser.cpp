#pragma once

#include <vector>
#include <stack>

#include "Serializers/JSONGenerator.cpp"
#include "NecessaryLibraryDetector.cpp"
#include "Lexer.cpp"

namespace Parsing {
	enum StackItemType {
		SITTerminal, // 0

		SITProgram, // 1

		SITFunctionDec, // 2
		SITFunctionBody, // 3
		SITFunction, // 4
		
		SITReturnStatement, // 5
		SITReturnVarStatement, // 6
		SITDeclareVarInitByLongLitStatement, // 7
		SITPrintNumStatement, // 8
		SITPrintVarStatement, // 9
		SITStatement, // 10
		SITStatements, // 11
		
		SITFunctionCall, // 12

		SITPlusEqualsNum, // 13
		SITMinusEqualsNum, // 14
		SITTimesEqualsNum, // 15
		SITDivideEqualsNum, // 16
		SITModEqualsNum, // 17

		SITPlusEqualsVar, // 18
		SITMinusEqualsVar, // 19
		SITTimesEqualsVar, // 20
		SITDivideEqualsVar, // 21
		SITModEqualsVar, // 22
	};

	typedef struct StackItem {
		StackItemType type;
		void* data;

		StackItem(){}

		StackItem(StackItemType _type, void* _data) {
			type = _type;
			data = _data;
		}
	} StackItem;

	enum StatementType {
		STDeclareVarInitByLongLitStatement,
		STReturnNumStatement,
		STReturnVarStatement,

		STPrintANumber,
		STPrintAVariable,
		
		STPlusEqualsANumber,
		STMinusEqualsANumber,
		STTimesEqualsANumber,
		STDivideEqualsANumber,
		STModEqualsANumber,

		STPlusEqualsAVariable,
		STMinusEqualsAVariable,
		STTimesEqualsAVariable,
		STDivideEqualsAVariable,
		STModEqualsAVariable,
	};

	typedef struct {
		long long returnValue;
	} ReturnNumStatement;

	typedef struct {
		std::string identifier;
	} ReturnVarStatement;

	typedef struct {
		std::string varType;
		std::string identifier;
		long long numValue;
	} DeclareVarInitByLongLitStatement;

	typedef struct {
		long long numValue;
	} PrintNumStatement;

	typedef struct {
		std::string identifier;
	} PrintVarStatement;

	typedef struct {
		std::string identifier;
		long long numValue;
	} PlusEqualsANumberStatement;

	typedef struct {
		std::string identifier;
		long long numValue;
	} MinusEqualsANumberStatement;

	typedef struct {
		std::string identifier;
		long long numValue;
	} TimesEqualsANumberStatement;

	typedef struct {
		std::string identifier;
		long long numValue;
	} DivideEqualsANumberStatement;

	typedef struct {
		std::string identifier;
		long long numValue;
	} ModEqualsANumberStatement;

	typedef struct {
		std::string resultIdentifier;
		std::string operandIdentifier;
	} PlusEqualsAVariableStatement;

	typedef struct {
		std::string resultIdentifier;
		std::string operandIdentifier;
	} MinusEqualsAVariableStatement;
	
	typedef struct {
		std::string resultIdentifier;
		std::string operandIdentifier;
	} TimesEqualsAVariableStatement;

	typedef struct {
		std::string resultIdentifier;
		std::string operandIdentifier;
	} DivideEqualsAVariableStatement;

	typedef struct {
		std::string resultIdentifier;
		std::string operandIdentifier;
	} ModEqualsAVariableStatement;
	
	typedef struct {
		StatementType type;
		
		DeclareVarInitByLongLitStatement declareAVarWithLongLit;
		
		ReturnNumStatement returnANumber;
		ReturnVarStatement returnAVariable;
		
		PrintNumStatement printANumber;
		PrintVarStatement printAVariable;

		PlusEqualsANumberStatement plusEqualsNum;
		MinusEqualsANumberStatement minusEqualsNum;
		TimesEqualsANumberStatement timesEqualsNum;
		DivideEqualsANumberStatement divideEqualsNum;
		ModEqualsANumberStatement modEqualsNum;

		PlusEqualsAVariableStatement plusEqualsVar;
		MinusEqualsAVariableStatement minusEqualsVar;
		TimesEqualsAVariableStatement timesEqualsVar;
		DivideEqualsAVariableStatement divideEqualsVar;
		ModEqualsAVariableStatement modEqualsVar;
	} Statement;

	typedef struct {
		void* nextStatements;
		Statement statement;
	} Statements;



	typedef struct {
		Statements statements;
	} FunctionBody;

	typedef struct {
		std::string functionName;
		std::string returnType;
	} FunctionDec;

	typedef struct {
		FunctionDec functionDec;
		FunctionBody functionBody;
	} Function;



	typedef struct {
		Function function;
	} Program;



	class Parser {
		ErrorHandling::ErrorHandler* err;
		SymbolTracking::SymbolTable* sym;
		LibraryDetecting::NeededLibraries* lib;
	public:
		Program program;

		Parser(ErrorHandling::ErrorHandler* errorHandler, SymbolTracking::SymbolTable* symbolTable, LibraryDetecting::NeededLibraries* neededLibraries){
			err = errorHandler;
			sym = symbolTable;
			lib = neededLibraries;
		}

		void Parse(std::vector<Lexing::Token*>* tokens){
			std::vector<StackItem>* parseStack = new std::vector<StackItem>();
			int pssize; // parse stack size - 1

			for (int currentToken = 0; currentToken < tokens->size(); currentToken++) {
				bool rulesFound;

				// push next token onto parse stack
				parseStack->push_back(*new StackItem(SITTerminal, (void*)(*tokens)[currentToken]));

				do {
					// parse stack size temp for indexing
					pssize = parseStack->size() - 1;
					rulesFound = false;

					// return statements
					#pragma region
					// search for rules
					// ReturnNumStatement
					if (parseStack->size() >= 3 &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTReturn &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						ReturnNumStatement* returnNumStatement = new ReturnNumStatement();
						returnNumStatement->returnValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 3);

						parseStack->push_back(StackItem(SITReturnStatement, returnNumStatement));

						continue;
					}
					// ReturnVarStatement
					if (parseStack->size() >= 3 &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTReturn &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						ReturnVarStatement* returnVarStatement = new ReturnVarStatement();
						returnVarStatement->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 3);

						parseStack->push_back(StackItem(SITReturnVarStatement, returnVarStatement));

						continue;
					}
					#pragma endregion
					// declare var
					#pragma region
					// DeclareVarInitByLongLitStatement
					if (parseStack->size() >= 5 &&
					(*parseStack)[pssize - 4].type == SITTerminal &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 4].data)).type == Lexing::TTLongDec &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTAssignment &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						DeclareVarInitByLongLitStatement* dviblls = new DeclareVarInitByLongLitStatement();
						dviblls->varType = "long";
						dviblls->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						dviblls->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 5);

						parseStack->push_back(StackItem(SITDeclareVarInitByLongLitStatement, dviblls));

						continue;
					}
					#pragma endregion
					// print statements
					#pragma region
					// Print (a number)
					if (parseStack->size() >= 5 &&
					(*parseStack)[pssize - 4].type == SITTerminal &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 4].data)).type == Lexing::TTPrint &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTOpenArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTCloseArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;
						lib->print = true;

						PrintNumStatement* printNumStatement = new PrintNumStatement();
						printNumStatement->numValue = (unsigned long long)(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).data;

						pop(parseStack, 5);
						parseStack->push_back(StackItem(SITPrintNumStatement, printNumStatement));

						continue;
					}
					// Print (a variable)
					if (parseStack->size() >= 5 &&
					(*parseStack)[pssize - 4].type == SITTerminal &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 4].data)).type == Lexing::TTPrint &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTOpenArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTCloseArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;
						lib->print = true;

						PrintVarStatement* printVarStatement = new PrintVarStatement();
						printVarStatement->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).data);

						pop(parseStack, 5);
						parseStack->push_back(StackItem(SITPrintVarStatement, printVarStatement));

						continue;
					}
					#pragma endregion
					// arithmetic
					#pragma region
					// Plus Equals (a number)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTPlusEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						PlusEqualsANumberStatement* peans = new PlusEqualsANumberStatement();
						peans->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						peans->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITPlusEqualsNum, peans));

						continue;
					}
					// Minus Equals (a number)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTMinusEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						MinusEqualsANumberStatement* means = new MinusEqualsANumberStatement();
						means->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						means->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITMinusEqualsNum, means));

						continue;
					}
					// Times Equals (a number)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTTimesEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						TimesEqualsANumberStatement* teans = new TimesEqualsANumberStatement();
						teans->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						teans->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITTimesEqualsNum, teans));

						continue;
					}
					// Divide Equals (a number)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTDivideByEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						DivideEqualsANumberStatement* deans = new DivideEqualsANumberStatement();
						deans->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						deans->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITDivideEqualsNum, deans));

						continue;
					}
					// Mod Equals (a number)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTModEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTLongLiteral &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						ModEqualsANumberStatement* means = new ModEqualsANumberStatement();
						means->identifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						means->numValue = (long long)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data;

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITModEqualsNum, means));

						continue;
					}
					// Plus Equals (a variable)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTPlusEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						PlusEqualsAVariableStatement* peavs = new PlusEqualsAVariableStatement();
						peavs->resultIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						peavs->operandIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITPlusEqualsVar, peavs));

						continue;
					}
					// Minus Equals (a variable)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTMinusEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						MinusEqualsAVariableStatement* meavs = new MinusEqualsAVariableStatement();
						meavs->resultIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						meavs->operandIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITMinusEqualsVar, meavs));

						continue;
					}
					// Times Equals (a variable)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTTimesEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						TimesEqualsAVariableStatement* teavs = new TimesEqualsAVariableStatement();
						teavs->resultIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						teavs->operandIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITTimesEqualsVar, teavs));

						continue;
					}
					// Divide Equals (a variable)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTDivideByEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						DivideEqualsAVariableStatement* deavs = new DivideEqualsAVariableStatement();
						deavs->resultIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						deavs->operandIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITDivideEqualsVar, deavs));

						continue;
					}
					// Mod Equals (a variable)
					if (parseStack->size() >= 4 &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTModEquals &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTIdentifier &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTEndStatement)
					{
						rulesFound = true;

						ModEqualsAVariableStatement* meavs = new ModEqualsAVariableStatement();
						meavs->resultIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).data);
						meavs->operandIdentifier = (*(std::string*)(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).data);

						pop(parseStack, 4);
						parseStack->push_back(StackItem(SITModEqualsVar, meavs));

						continue;
					}
					#pragma endregion
					// statement types
					#pragma region
					// Statement (from returning a number)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITReturnStatement)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STReturnNumStatement;
						statement->returnANumber = (*(ReturnNumStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from returning a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITReturnVarStatement)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STReturnVarStatement;
						statement->returnAVariable = (*(ReturnVarStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from declaring a variable with a long assignment)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITDeclareVarInitByLongLitStatement)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STDeclareVarInitByLongLitStatement;
						statement->declareAVarWithLongLit = (*(DeclareVarInitByLongLitStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from printing a num)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITPrintNumStatement)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STPrintANumber;
						statement->printANumber = (*(PrintNumStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from printing a var)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITPrintVarStatement)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STPrintAVariable;
						statement->printAVariable = (*(PrintVarStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from adding a number to a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITPlusEqualsNum)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STPlusEqualsANumber;
						statement->plusEqualsNum = (*(PlusEqualsANumberStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from subtracting a number from a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITMinusEqualsNum)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STMinusEqualsANumber;
						statement->minusEqualsNum = (*(MinusEqualsANumberStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from multiplying a variable by a number)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITTimesEqualsNum)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STTimesEqualsANumber;
						statement->timesEqualsNum = (*(TimesEqualsANumberStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from dividing a variable by a number)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITDivideEqualsNum)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STDivideEqualsANumber;
						statement->divideEqualsNum = (*(DivideEqualsANumberStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));
						
						continue;
					}
					// Statement (from modding a variable by a number)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITModEqualsNum)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STModEqualsANumber;
						statement->modEqualsNum = (*(ModEqualsANumberStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from adding a variable to a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITPlusEqualsVar)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STPlusEqualsAVariable;
						statement->plusEqualsVar = (*(PlusEqualsAVariableStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from subtracting a variable from a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITMinusEqualsVar)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STMinusEqualsAVariable;
						statement->minusEqualsVar = (*(MinusEqualsAVariableStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from multiplying a variable by a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITTimesEqualsVar)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STTimesEqualsAVariable;
						statement->timesEqualsVar = (*(TimesEqualsAVariableStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from dividing a variable by a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITDivideEqualsVar)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STDivideEqualsAVariable;
						statement->divideEqualsVar = (*(DivideEqualsAVariableStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					// Statement (from modding a variable by a variable)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITModEqualsVar)
					{
						rulesFound = true;

						Statement* statement = new Statement();
						statement->type = STModEqualsAVariable;
						statement->modEqualsVar = (*(ModEqualsAVariableStatement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatement, statement));

						continue;
					}
					#pragma endregion
					// statements types
					#pragma region
					// Statements (from one statements and one statement)
					if (parseStack->size() >= 2 &&
					(*parseStack)[pssize - 1].type == SITStatements &&
					(*parseStack)[pssize - 0].type == SITStatement)
					{
						rulesFound = true;

						Statements* statements = new Statements();
						statements->nextStatements = 0;
						statements->statement = (*(Statement*)((*parseStack)[pssize].data));

						Statements* tempStatements = (Statements*)((*parseStack)[pssize - 1].data);
						while (tempStatements->nextStatements != 0) {
							tempStatements = (Statements*)tempStatements->nextStatements;
						}
						tempStatements->nextStatements = statements;

						pop(parseStack, 1);

						continue;
					}
					// Statements (from one statement)
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITStatement)
					{
						rulesFound = true;

						Statements* statements = new Statements();
						statements->nextStatements = (Statement*)0;
						statements->statement = (*(Statement*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITStatements, statements));

						continue;
					}
					#pragma endregion
					// Function misc
					#pragma region
					// FunctionBody
					if (parseStack->size() >= 3 &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITStatements &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTOpenFuncBody &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTCloseFuncBody)
					{
						rulesFound = true;

						FunctionBody* functionBody = new FunctionBody();
						functionBody->statements = (*(Statements*)((*parseStack)[pssize - 1].data));

						pop(parseStack, 3);
						parseStack->push_back(StackItem(SITFunctionBody, functionBody));

						continue;
					}
					// FunctionDec
					if (parseStack->size() >= 5 &&
					(*parseStack)[pssize - 4].type == SITTerminal &&
					(*parseStack)[pssize - 3].type == SITTerminal &&
					(*parseStack)[pssize - 2].type == SITTerminal &&
					(*parseStack)[pssize - 1].type == SITTerminal &&
					(*parseStack)[pssize - 0].type == SITTerminal &&
					(*(Lexing::Token*)((*parseStack)[pssize - 4].data)).type == Lexing::TTFunctionDec &&
					(*(Lexing::Token*)((*parseStack)[pssize - 3].data)).type == Lexing::TTMain &&
					(*(Lexing::Token*)((*parseStack)[pssize - 2].data)).type == Lexing::TTOpenArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 1].data)).type == Lexing::TTCloseArgs &&
					(*(Lexing::Token*)((*parseStack)[pssize - 0].data)).type == Lexing::TTLongDec)
					{
						rulesFound = true;

						FunctionDec* funcDec = new FunctionDec();
						funcDec->functionName = "main"; // needs to be changed!
						funcDec->returnType = "long";

						pop(parseStack, 5);
						parseStack->push_back(StackItem(SITFunctionDec, funcDec));

						continue;
					}
					// Function
					if (parseStack->size() >= 2 &&
					(*parseStack)[pssize - 1].type == SITFunctionDec &&
					(*parseStack)[pssize - 0].type == SITFunctionBody)
					{
						rulesFound = true;

						Function* function = new Function();
						function->functionDec = (*(FunctionDec*)((*parseStack)[pssize - 1].data));
						function->functionBody = (*(FunctionBody*)((*parseStack)[pssize].data));

						pop(parseStack, 2);
						parseStack->push_back(StackItem(SITFunction, function));

						continue;
					}
					#pragma endregion
					// Program
					if (parseStack->size() >= 1 &&
					(*parseStack)[pssize - 0].type == SITFunction)
					{
						rulesFound = true;

						Program* program = new Program();
						program->function = (*(Function*)((*parseStack)[pssize].data));

						pop(parseStack, 1);
						parseStack->push_back(StackItem(SITProgram, program));

						continue;
					}
				} while (rulesFound);
			}

			// determine if successful
			if (parseStack->size() == 1 && (*parseStack)[0].type == SITProgram){
				program = (*(Program*)((*parseStack)[0].data));
			} else {
				for (int i = 0; i < parseStack->size(); i++){
					if ((*parseStack)[i].type == SITTerminal) {
						if (((Lexing::Token*)parseStack->operator[](i).data)->type != Lexing::TokenType::TTIdentifier) {
							std::cout << ":" << (char*)(*(Lexing::Token*)(*parseStack)[i].data).data << ": ";
						} else {
							std::cout << ":" << (*(std::string*)(*(Lexing::Token*)(*parseStack)[i].data).data) << ": ";
						}
					} else {
						std::cout << (*parseStack)[i].type << " ";
					}
				}
				std::cout << std::endl;



				err->ThrowError(ErrorHandling::ErrorType::ETParsingIncomplete, "Input token stream did not reduce to a program object. Compilation Stopped.", "null", 0);
			}
		}

		void pop(std::vector<StackItem>* parseStack, int times) {
			for (int i = 0; i < times; i++) {
				parseStack->pop_back();
			}
		}

	public:
		std::string GenerateJSON(){
			JSONGenerating::JSONGenerator jsonGen = JSONGenerating::JSONGenerator();

			// generate json line by line
			jsonGen.MakeOpen("{");
			jsonGen.MakeLine("program", "{");
			jsonGen.MakeLine("function", "{");
			jsonGen.MakeLine("functionDec", "{");
			jsonGen.MakeVariable("functionName", program.function.functionDec.functionName, JSONGenerating::JSONVarType::JVTString, true);
			jsonGen.MakeVariable("returnType", program.function.functionDec.returnType, JSONGenerating::JSONVarType::JVTString, false);
			jsonGen.MakeClose("}", true);
			jsonGen.MakeLine("functionBody", "{");
			jsonGen.MakeLine("statements", "{");

			Statements* tempStatements = &program.function.functionBody.statements;
			for (int i = 0; tempStatements != 0; i++) {
				jsonGen.MakeLine("statement", "{");
				switch (tempStatements->statement.type) {
				case STReturnNumStatement:
					jsonGen.MakeLine("returnANumber", "{");
					jsonGen.MakeVariable("returnValue", std::to_string(tempStatements->statement.returnANumber.returnValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STReturnVarStatement:
					jsonGen.MakeLine("returnAVariable", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.returnAVariable.identifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STDeclareVarInitByLongLitStatement:
					jsonGen.MakeLine("declareVarInitByLongLitStatement", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.declareAVarWithLongLit.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.declareAVarWithLongLit.numValue), JSONGenerating::JSONVarType::JVTNumber, true);
					jsonGen.MakeVariable("varType", tempStatements->statement.declareAVarWithLongLit.varType, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STPrintANumber:
					jsonGen.MakeLine("printANumber", "{");
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.printANumber.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STPrintAVariable:
					jsonGen.MakeLine("printAVariable", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.printAVariable.identifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STPlusEqualsANumber:
					jsonGen.MakeLine("plusEqualsNum", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.plusEqualsNum.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.plusEqualsNum.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STMinusEqualsANumber:
					jsonGen.MakeLine("minusEqualsNum", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.minusEqualsNum.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.minusEqualsNum.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STTimesEqualsANumber:
					jsonGen.MakeLine("timesEqualsNum", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.timesEqualsNum.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.timesEqualsNum.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STDivideEqualsANumber:
					jsonGen.MakeLine("divideEqualsNum", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.divideEqualsNum.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.divideEqualsNum.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STModEqualsANumber:
					jsonGen.MakeLine("modEqualsNum", "{");
					jsonGen.MakeVariable("identifier", tempStatements->statement.modEqualsNum.identifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("numValue", std::to_string(tempStatements->statement.modEqualsNum.numValue), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case STPlusEqualsAVariable:
					jsonGen.MakeLine("plusEqualsVar", "{");
					jsonGen.MakeVariable("resultIdentifier", tempStatements->statement.plusEqualsVar.resultIdentifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("operandIdentifier", tempStatements->statement.plusEqualsVar.operandIdentifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STMinusEqualsAVariable:
					jsonGen.MakeLine("minusEqualsVar", "{");
					jsonGen.MakeVariable("resultIdentifier", tempStatements->statement.minusEqualsVar.resultIdentifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("operandIdentifier", tempStatements->statement.minusEqualsVar.operandIdentifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STTimesEqualsAVariable:
					jsonGen.MakeLine("timesEqualsVar", "{");
					jsonGen.MakeVariable("resultIdentifier", tempStatements->statement.timesEqualsVar.resultIdentifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("operandIdentifier", tempStatements->statement.timesEqualsVar.operandIdentifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STDivideEqualsAVariable:
					jsonGen.MakeLine("divideEqualsVar", "{");
					jsonGen.MakeVariable("resultIdentifier", tempStatements->statement.divideEqualsVar.resultIdentifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("operandIdentifier", tempStatements->statement.divideEqualsVar.operandIdentifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				case STModEqualsAVariable:
					jsonGen.MakeLine("modEqualsVar", "{");
					jsonGen.MakeVariable("resultIdentifier", tempStatements->statement.modEqualsVar.resultIdentifier, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("operandIdentifier", tempStatements->statement.modEqualsVar.operandIdentifier, JSONGenerating::JSONVarType::JVTString, false);
					jsonGen.MakeClose("}", false);
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(tempStatements->statement.type), "StatementType", (void*)tempStatements->statement.type);
				}
				jsonGen.MakeClose("}", (tempStatements->nextStatements != 0) ? true : false);

				tempStatements = (Statements*)tempStatements->nextStatements;
			}
			
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);

			return jsonGen.JSON;
		}
	};
}
