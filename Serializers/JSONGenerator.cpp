#pragma once

#include <iostream>
#include <vector>

namespace JSONGenerating {
		enum JSONVarType {
			JVTNumber,
			JVTString,
			JVTNull
		};

	class JSONGenerator {
	public:
		std::string JSON;
		int tabDepth;

		JSONGenerator() {
			JSON = "";
			tabDepth = 0;
		}

		std::string Tabs(int count){
			std::string output = "";

			for (int i = 0; i < count; i++) {
				output += "  ";
			}

			return output;
		}

		void MakeLine(std::string name, std::string bracket){
			JSON += Tabs(tabDepth) + "\"" + name + "\": " + bracket + "\n";
			tabDepth++;
		}

		void MakeVariable(std::string name, std::string variable, JSONVarType varType, bool endComma) {
			JSON += Tabs(tabDepth) + "\"" + name + "\": ";

			switch (varType) {
				case JVTNumber:
					JSON += variable;
					break;
				case JVTString:
					JSON += "\"" + variable + "\"";
					break;
				case JVTNull:
					JSON += "null";
					break;
			}

			if (endComma) {
				JSON += ",";
			}

			JSON += "\n";
		}

		void MakeOpen(std::string bracket){
			JSON += Tabs(tabDepth) + bracket + "\n";
			tabDepth++;
		}

		void MakeClose(std::string bracket, bool endComma){
			tabDepth--;

			JSON += Tabs(tabDepth) + bracket;

			if (endComma) {
				JSON += ",";
			}

			JSON += "\n";
		}
	};
}
