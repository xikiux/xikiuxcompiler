#pragma once

#include <iostream>
#include <string>

namespace ErrorHandling {
    enum ErrorType {
        ETNOERROR,
        ETNoCommandLineArgs,
        ETNotImplementedException,
        ETSourceFileNotFound,
        ETNoTokenFound,
        ETParsingIncomplete,
        ETAbstractingIncomplete,
        ETIdentifierAlreadyDeclared,
        ETIdentifierUsedButNotDeclared,
        ETJsonToStringFailure
    };

    typedef struct {
        ErrorType type;
        std::string message;
        std::string errorDataType;
        void* errorData;
    } Error;
    
    class ErrorHandler {
        Error error;
    public:
        ErrorHandler(){}

        void ThrowError(ErrorType errorType, std::string message, std::string errorDataType, void* errorData){
            error = Error();

            error.type = errorType;
            error.message = message;
            error.errorDataType = errorDataType;
            error.errorData = errorData;

            std::cerr << "Error thrown: " << message << std::endl;
            std::cerr << "Error type: " << ErrorTypeToString(error.type) << std::endl;
            std::cerr << "Error data type: " << error.errorDataType << std::endl;
            std::cerr << "Error data pointer: " << error.errorData << std::endl;

            exit(error.type);
        }

        void ThrowHelpScreen(){
            std::cerr << "Help:" << std::endl;
            std::cerr << "./Compiler [file path]" << std::endl;
            std::cerr << "./Compiler [file path] [output file name]" << std::endl;
        }

    private:
        std::string ErrorTypeToString(ErrorType type) {
            switch (type)
            {
            case ETNOERROR:
                return "ETNOERROR";
            case ETNoCommandLineArgs:
                return "ETNoCommandLineArgs";
            case ETNotImplementedException:
                return "ETNotImplementedException";
            case ETSourceFileNotFound:
                return "ETSourceFileNotFound";
            case ETNoTokenFound:
                return "ETNoTokenFound";
            case ETParsingIncomplete:
                return "ETParsingIncomplete";
            case ETAbstractingIncomplete:
                return "ETAbstractingIncomplete";
            case ETIdentifierAlreadyDeclared:
                return "ETIdentifierAlreadyDeclared";
            case ETIdentifierUsedButNotDeclared:
                return "ETIdentifierUsedButNotDeclared";
            case ETJsonToStringFailure:
                return "ETJsonToStringFailure";
            default:
                std::cerr << "In " << __func__ << ", error unidentified, but caught." << std::endl; 
                exit(-2);
            }
        }
    };
}