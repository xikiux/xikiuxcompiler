#pragma once

#include <vector>

namespace Dictionaries {
    template<typename KEYTYPE, typename VALUETYPE>
    class Dictionary {
        std::vector<KEYTYPE> keys;
        std::vector<VALUETYPE> values;
    public:
        Dictionary(){
            keys = std::vector<KEYTYPE>();
            values = std::vector<VALUETYPE>();
        }

        int Count(){
            return keys.size();
        }

        bool ContainsKey(KEYTYPE _key){
            for (int i = 0; i < keys.size(); i++) {
                if (keys[i] == _key) {
                    return true;
                }
            }

            return false;
        }

        bool ContainsValue(VALUETYPE _value){
            for (int i = 0; i < keys.size(); i++) {
                if (values[i] == _value) {
                    return true;
                }
            }

            return false;
        }
        
        void Insert(KEYTYPE _key, VALUETYPE _value){
            if (!ContainsKey(_key)) {
                keys.push_back(_key);
                values.push_back(_value);
            }
        }
        
        int IndexOfKey(KEYTYPE _key){
            for (int i = 0; i < keys.size(); i++) {
                if (keys[i] == _key) {
                    return i;
                }
            }

            return -1;
        }
        
        int IndexOfValue(VALUETYPE _value){
            for (int i = 0; i < keys.size(); i++) {
                if (values[i] == _value) {
                    return i;
                }
            }

            return -1;
        }
        
        void Remove(KEYTYPE _key){
            int index = IndexOfKey(_key);

            keys.erase(index);
            values.erase(index);
        }
        
        void Remove(int index){
            keys.erase(index);
            values.erase(index);
        }
        
        VALUETYPE operator[](KEYTYPE _key){
            int indexOfKey = IndexOfKey(_key);
            
            if (indexOfKey != -1) {
                return values[indexOfKey];
            } else {
                std::cout << "Dictionary.cpp: Error: Attempt to request value of non-existant key. Execution cannot continue, stopping..." << std::endl;
                exit(-1);
            }
        }
    };
}