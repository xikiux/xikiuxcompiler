#pragma once

#include <fstream>

#include "../Error/ErrorHandler.cpp"

std::string ReadFileIntoString(ErrorHandling::ErrorHandler* err, std::string filePath) {
	std::string program = "";
	std::ifstream file;

	file.open(filePath);

	if (file.is_open()){
		program.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
	}else{
		err->ThrowError(ErrorHandling::ErrorType::ETSourceFileNotFound, "File " + filePath + " could not be opened for reading!", "null", 0);
	}

	file.close();

	return program;
}

bool WriteStringToFile(std::string fileName, std::string data) {
	std::ofstream file;
	
	file.open(fileName);

	if (file.is_open()) {
		file << data;
	}else{
		std::cout << "File \"" + fileName + "\" not opened!\n" << std::endl;
		return false;
	}

	file.close();

	return true;
}