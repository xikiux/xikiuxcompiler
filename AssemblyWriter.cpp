#pragma once

#include <iostream>

#include "Lib/Dictionary.cpp"
#include "Lib/FileIO.cpp"

#include "IntermediateCoder.cpp"
#include "NecessaryLibraryDetector.cpp"

namespace AssemblyWriting {
	enum InstructionType {
		ITpush,
		ITpop,
		ITmov,
		ITsyscall,
		ITcall,
		ITadd,
		ITsub,
		ITmul,
		ITdiv,
		ITmod
	};

	enum MemoryLocation {
		MLrax,
		MLrbx,
		MLrcx,
		MLrdx,
		MLrsi,
		MLrdi,
		MLrbp,
		MLrsp,
		MLr8,
		MLr9,
		MLr10,
		MLr11,
		MLr12,
		MLr13,
		MLr14,
		MLr15,
		MLStack
	};

	enum InstructionArgumentType {
		IATMemoryLocationWithAddition,
		IATMemoryLocation,
		IATRegister,
		IATValue,
		IATLabel
	};

	class AssemblyWriter {
		ErrorHandling::ErrorHandler* err;
		SymbolTracking::SymbolTable* sym;
		LibraryDetecting::NeededLibraries* lib;
	public:
		std::string assemblyCode;
		bool success;

		AssemblyWriter(ErrorHandling::ErrorHandler* errorHandler, SymbolTracking::SymbolTable* symbolTable, LibraryDetecting::NeededLibraries* neededLibraries){
			err = errorHandler;
			sym = symbolTable;
			lib = neededLibraries;

			success = false;
		}

		typedef struct InstructionArgument {
			InstructionArgumentType type;
			MemoryLocation address;
			long long value;

			InstructionArgument() {}

			InstructionArgument(InstructionArgumentType _type, MemoryLocation _address) {
				type = _type;
				address = _address;
			}

			InstructionArgument(InstructionArgumentType _type, long long _value) {
				type = _type;
				value = _value;
			}
		} InstructionArgument;

		typedef struct AssemblyInstruction {
			InstructionType operation;
			InstructionArgument arg1;
			InstructionArgument arg2;

			AssemblyInstruction(InstructionType _operation) {
				operation = _operation;
			}

			AssemblyInstruction(InstructionType _operation, InstructionArgument _arg1) {
				operation = _operation;
				arg1 = _arg1;
			}

			AssemblyInstruction(InstructionType _operation, InstructionArgument _arg1, InstructionArgument _arg2) {
				operation = _operation;
				arg1 = _arg1;
				arg2 = _arg2;
			}
		} AssemblyInstruction;

		void WriteAssemblyCode(IntermediateCoding::IntermediateCode* ic) {
			std::vector<AssemblyInstruction>* unserializedAssemblyCode = new std::vector<AssemblyInstruction>();
			Dictionaries::Dictionary<long long, MemoryLocation> tempRegToRealMemoryMap = Dictionaries::Dictionary<long long, MemoryLocation>();

			// generate assembly
			for (int i = 0; i < ic->code.size(); i++) {
				switch (ic->code[i].op) {
				case IntermediateCoding::Operation::ONOP:
					err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "instruction ONOP hasn't been implemented", "null", 0);
					break;
				
				case IntermediateCoding::Operation::OReturnValue:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATValue, 60))); // mov rax, 60
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdi), InstructionArgument(IATValue, (unsigned long long)ic->code[i].firstOperand.data))); // mov rdi, return value
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITsyscall)); // syscall
					break;
				case IntermediateCoding::Operation::OReturnTempRegister:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATValue, 60))); // mov rax, 60
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdi), InstructionArgument(IATRegister, tempRegToRealMemoryMap[(unsigned long long)ic->code[i].firstOperand.data]))); // mov rdi, temp register
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITsyscall)); // syscall
					break;
				
				case IntermediateCoding::Operation::OCreateTempRegInitByLongLit:
					if (tempRegToRealMemoryMap.Count() < 8) {
						tempRegToRealMemoryMap.Insert(tempRegToRealMemoryMap.Count(), MapTempRegWithRealMemory((unsigned long long)ic->code[i].resultOperand.data));
						unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MapTempRegWithRealMemory((unsigned long long)tempRegToRealMemoryMap.Count() - 1)), InstructionArgument(IATValue, (long long)ic->code[i].firstOperand.data)));
					} else {
						err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "Pushing variables onto the stack hasn't been implemented (you have exceeded 8 registers (variables)).", "null", 0);
					}

					break;
				
				case IntermediateCoding::Operation::OPrintLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITpush, InstructionArgument(IATValue, (long long)ic->code[i].firstOperand.data))); // push value
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITcall, InstructionArgument(IATLabel, (long long)"_LTS")));
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITadd, InstructionArgument(IATRegister, MLrsp), InstructionArgument(IATValue, 8)));
					break;
				case IntermediateCoding::Operation::OPrintTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITpush, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data]))); // push register
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITcall, InstructionArgument(IATLabel, (long long)"_LTS")));
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITadd, InstructionArgument(IATRegister, MLrsp), InstructionArgument(IATValue, 8)));
					break;
				
				case IntermediateCoding::Operation::OPlusEqualTempRegByLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITadd, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATValue, (long long)ic->code[i].firstOperand.data)));
					break;
				case IntermediateCoding::Operation::OMinusEqualTempRegByLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITsub, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATValue, (long long)ic->code[i].firstOperand.data)));
					break;
				case IntermediateCoding::Operation::OTimesEqualTempRegByLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdx), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATValue,  (long long)ic->code[i].firstOperand.data))); // mov num to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmul, InstructionArgument(IATRegister, MLrdx))); // multiply by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrax))); // mov back to register
					break;
				case IntermediateCoding::Operation::ODivideEqualTempRegByLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrcx), InstructionArgument(IATValue,  (long long)ic->code[i].firstOperand.data))); // mov num to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITdiv, InstructionArgument(IATRegister, MLrcx))); // divide by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrax))); // mov back to register
					break;
				case IntermediateCoding::Operation::OModEqualTempRegByLongLit:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrcx), InstructionArgument(IATValue,  (long long)ic->code[i].firstOperand.data))); // mov num to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITdiv, InstructionArgument(IATRegister, MLrcx))); // divide by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrdx))); // mov remainder to register
					break;

				case IntermediateCoding::Operation::OPlusEqualTempRegByTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITadd, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data])));
					break;
				case IntermediateCoding::Operation::OMinusEqualTempRegByTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITsub, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data])));
					break;
				case IntermediateCoding::Operation::OTimesEqualTempRegByTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdx), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data]))); // mov variable to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmul, InstructionArgument(IATRegister, MLrdx))); // multiply by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrax))); // mov back to register
					break;
				case IntermediateCoding::Operation::ODivideEqualTempRegByTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrcx), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data]))); // mov variable to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdx), InstructionArgument(IATValue, 0))); // clear rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITdiv, InstructionArgument(IATRegister, MLrcx))); // divide by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrax))); // mov back to register
					break;
				case IntermediateCoding::Operation::OModEqualTempRegByTempReg:
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrax), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]))); // mov variable to rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrcx), InstructionArgument(IATRegister,  tempRegToRealMemoryMap[(long long)ic->code[i].firstOperand.data]))); // mov variable to rax
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, MLrdx), InstructionArgument(IATValue, 0))); // clear rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITdiv, InstructionArgument(IATRegister, MLrcx))); // divide by rdx
					unserializedAssemblyCode->push_back(AssemblyInstruction(ITmov, InstructionArgument(IATRegister, tempRegToRealMemoryMap[(long long)ic->code[i].resultOperand.data]), InstructionArgument(IATRegister, MLrdx))); // mov remainder to register
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, std::string("In " + std::string(__func__) + " no intermediate to assembly was implemented."), "IntermediateCoding::Operation", (void*)(long long)ic->code[i].op);
				}
			}

			// serialize
			assemblyCode = "global _start\n\nsection .text\n\n_start:\n";

			for (int i = 0; i < unserializedAssemblyCode->size(); i++) {
				AssemblyInstruction tempAI = (*unserializedAssemblyCode)[i];
				
				assemblyCode += "\t";

				switch (tempAI.operation) {
				case ITmov:
					assemblyCode += "mov " + InstructionArgumentToString(tempAI.arg1) + ", " + InstructionArgumentToString(tempAI.arg2);
					break;
				case ITpush:
					assemblyCode += "push " + InstructionArgumentToString(tempAI.arg1);
					break;
				case ITsyscall:
					assemblyCode += "syscall";
					break;
				case ITcall:
					assemblyCode += "call " + std::string((const char*)tempAI.arg1.value);
					break;
				case ITadd:
					assemblyCode += "add " + InstructionArgumentToString(tempAI.arg1) + ", " + InstructionArgumentToString(tempAI.arg2);
					break;
				case ITsub:
					assemblyCode += "sub " + InstructionArgumentToString(tempAI.arg1) + ", " + InstructionArgumentToString(tempAI.arg2);
					break;
				case ITmul:
					assemblyCode += "mul " + InstructionArgumentToString(tempAI.arg1);
					break;
				case ITdiv:
				case ITmod:
					assemblyCode += "div " + InstructionArgumentToString(tempAI.arg1);
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "In AssemblyWriter.cpp, function WriteAssemblyCode, operation not implemented.", "InstructionType", (void*)tempAI.operation);
				}

				assemblyCode += "\n";
			}

			assemblyCode += AddDependencies();
		}
	private:
		MemoryLocation MapTempRegWithRealMemory(unsigned long long tempReg){
			switch (tempReg)
			{
			case 0:
				return MLr8;
			case 1:
				return MLr9;
			case 2:
				return MLr10;
			case 3:
				return MLr11;
			case 4:
				return MLr12;
			case 5:
				return MLr13;
			case 6:
				return MLr14;
			case 7:
				return MLr15;
			default:
				return MLStack;
			}
		}

		std::string InstructionArgumentToString(InstructionArgument arg){
			switch (arg.type)
			{
			case IATRegister:
				switch (arg.address)
				{
				case MLrax:
					return "rax";
				case MLrcx:
					return "rcx";
				case MLrdx:
					return "rdx";
				case MLrdi:
					return "rdi";
				case MLrsp:
					return "rsp";
				case MLr8:
					return "r8";
				case MLr9:
					return "r9";
				case MLr10:
					return "r10";
				case MLr11:
					return "r11";
				case MLr12:
					return "r12";
				case MLr13:
					return "r13";
				case MLr14:
					return "r14";
				case MLr15:
					return "r15";
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "This register has not been implemented yet.", "register number", (void*)(long long)arg.address);
				}
				break;
			case IATValue:
				return std::to_string(arg.value);
			case IATMemoryLocation:
			case IATMemoryLocationWithAddition:
				err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "This argument type is not implemented yet.", "null", 0);
			}
		}

		std::string AddDependencies() {
			std::string output = "";

			if (lib->print) {
				output += ReadFileIntoString(err, "AsmLib/asmLongToString.s") + "\n\n";
			}

			return output;
		}
	};
}
