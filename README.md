xikiuxcompiler

The programming language that this compiler compilers does very little.<br/>
And it's extremely bare minimum so that I could learn about every stage of compiler.<br/>
This was just an exercise.<br/>
ALSO, I am no professional and this is not my cleanest work in some areas, so please don't judge to harshly though constructive criticism is welcomed.<br/>



The language:<br/>
Everything must be contained in one function, main.<br/>
Comments do not exist yet.<br/>
And main takes zero arguments and returns one signed or unsigned long (64 bit).<br/>

function main() long { STATEMENTS GO HERE }

Available statements:<br/>
long IDENTIFIERHERE = LONGLITERALHERE ;<br/>
IDENTIFIERHERE OPERATORHERE LONGLITERALHERE ;<br/>
IDENTIFIERHERE OPERATORHERE IDENTIFIERHERE ;<br/>
print(LONGLITERALHERE);<br/>
print(IDENTIFIERHERE);<br/>
return(LONGLITERALHERE);<br/>
return(IDENTIFIERHERE);<br/>

Identifiers are one or more lowercase/uppercase alphabetical characters, including underscores but excluding numbers

Available operators:<br/>
+=<br/>
-=<br/>
*=<br/>
/=<br/>
%=<br/>

And thats about it for now.<br/>
And there are no external libraries that need to be linked, thats taken care of for you.



How to build the compiler:<br/>
Required programs:<br/>
make<br/>
g++<br/>
ld

Making the compiler by command line:<br/>
make<br/>
./Compiler Programs/PROGRAMNAME [optional name instead of output]<br/>
./output [or whatever name you gave it]


And thats it. (It links the program for you).

Feedback is appreciated :)

xikiux