#pragma once

#include <vector>

#include "Lib/Dictionary.cpp"
#include "Serializers/JSONGenerator.cpp"

#include "Abstracter.cpp"

namespace IntermediateCoding {
	enum Operation {
		ONOP,
		
		OReturnValue,
		OReturnTempRegister,
		
		OCreateTempRegInitByLongLit,
		
		OPrintLongLit,
		OPrintTempReg,
		
		OPlusEqualTempRegByLongLit,
		OMinusEqualTempRegByLongLit,
		OTimesEqualTempRegByLongLit,
		ODivideEqualTempRegByLongLit,
		OModEqualTempRegByLongLit,

		OPlusEqualTempRegByTempReg,
		OMinusEqualTempRegByTempReg,
		OTimesEqualTempRegByTempReg,
		ODivideEqualTempRegByTempReg,
		OModEqualTempRegByTempReg,
	};

	enum TACOperandType {
		TACOTLong,
		TACOTTempRegister
	};

	typedef struct {
		TACOperandType type;
		void* data;
	} Operand;

	typedef struct {
		Operation op;
		Operand resultOperand;
		Operand firstOperand;
		Operand secondOperand;
	} ThreeAddressCode;

	typedef struct {
		int tempRegisterCount;
		std::vector<ThreeAddressCode> code;
	} IntermediateCode;

	class IntermediateCoder {
		ErrorHandling::ErrorHandler* err;
		SymbolTracking::SymbolTable* sym;
	public:
		IntermediateCode intermediateCode;

		IntermediateCoder(ErrorHandling::ErrorHandler* errorHandler, SymbolTracking::SymbolTable* symbolTable) {
			err = errorHandler;
			sym = symbolTable;
		}

		void Intermediate(Abstracting::AST* ast) {
			Dictionaries::Dictionary<std::string, long long> varToRegisterTable = Dictionaries::Dictionary<std::string, long long>();
			long long nextTempRegister = 0;
			
			intermediateCode.code = std::vector<ThreeAddressCode>();

			for (int i = 0; i < ast->function.statements.size(); i++) {
				ThreeAddressCode tempTAC = ThreeAddressCode();
				
				switch (ast->function.statements[i].type) {
				case Abstracting::StatementType::ASTReturnNum:
					tempTAC.op = Operation::OReturnValue;
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTReturnVar:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OReturnTempRegister;
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					break;
				case Abstracting::StatementType::ASTDeclareVarInitByLongLit:
					varToRegisterTable.Insert(ast->function.statements[i].resultVar, nextTempRegister);
					
					tempTAC.op = Operation::OCreateTempRegInitByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)nextTempRegister;
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					
					nextTempRegister++;
					break;
				case Abstracting::StatementType::ASTPrintANum:
					tempTAC.op = OPrintLongLit;
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTPrintAVar:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OPrintTempReg;
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					break;
				case Abstracting::StatementType::ASTPlusEqualsANumber:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OPlusEqualTempRegByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTMinusEqualsANumber:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OMinusEqualTempRegByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTTimesEqualsANumber:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OTimesEqualTempRegByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTDivideEqualsANumber:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = ODivideEqualTempRegByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTModEqualsANumber:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);

					tempTAC.op = OModEqualTempRegByLongLit;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTLong;
					tempTAC.firstOperand.data = (void*)ast->function.statements[i].firstOperandNum;
					break;
				case Abstracting::StatementType::ASTPlusEqualsAVariable:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);
					if (!sym->ContainsSymbol(ast->function.statements[i].firstOperandVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].firstOperandVar + "' is used but never declared", "null", 0);

					tempTAC.op = OPlusEqualTempRegByTempReg;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].firstOperandVar];
					break;
				case Abstracting::StatementType::ASTMinusEqualsAVariable:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);
					if (!sym->ContainsSymbol(ast->function.statements[i].firstOperandVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].firstOperandVar + "' is used but never declared", "null", 0);

					tempTAC.op = OMinusEqualTempRegByTempReg;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].firstOperandVar];
					break;
				case Abstracting::StatementType::ASTTimesEqualsAVariable:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);
					if (!sym->ContainsSymbol(ast->function.statements[i].firstOperandVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].firstOperandVar + "' is used but never declared", "null", 0);

					tempTAC.op = OTimesEqualTempRegByTempReg;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].firstOperandVar];
					break;
				case Abstracting::StatementType::ASTDivideEqualsAVariable:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);
					if (!sym->ContainsSymbol(ast->function.statements[i].firstOperandVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].firstOperandVar + "' is used but never declared", "null", 0);

					tempTAC.op = ODivideEqualTempRegByTempReg;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].firstOperandVar];
					break;
				case Abstracting::StatementType::ASTModEqualsAVariable:
					if (!sym->ContainsSymbol(ast->function.statements[i].resultVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].resultVar + "' is used but never declared", "null", 0);
					if (!sym->ContainsSymbol(ast->function.statements[i].firstOperandVar)) err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier '" + ast->function.statements[i].firstOperandVar + "' is used but never declared", "null", 0);

					tempTAC.op = OModEqualTempRegByTempReg;
					tempTAC.resultOperand.type = TACOTTempRegister;
					tempTAC.resultOperand.data = (void*)varToRegisterTable[ast->function.statements[i].resultVar];
					tempTAC.firstOperand.type = TACOTTempRegister;
					tempTAC.firstOperand.data = (void*)varToRegisterTable[ast->function.statements[i].firstOperandVar];
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETNotImplementedException, "In the intermediate coder, function Intermediate, an unidentified statement type was detected.", "ASTtype", (void*)ast->function.statements[i].type);
				}

				intermediateCode.code.push_back(tempTAC);
			}

			intermediateCode.tempRegisterCount = nextTempRegister;
		}

		std::string GenerateJSON() {
			JSONGenerating::JSONGenerator jsonGen = JSONGenerating::JSONGenerator();

			jsonGen.MakeOpen("{");
			jsonGen.MakeLine("intermediateCode", "{");
			jsonGen.MakeLine("code", "[");

			for (int i = 0; i < intermediateCode.code.size(); i++) {
				jsonGen.MakeOpen("{");

				jsonGen.MakeVariable("op", JSONOperationToString(intermediateCode.code[i].op), JSONGenerating::JSONVarType::JVTString, true);
				switch (intermediateCode.code[i].op) {
				case OReturnValue:
				case OReturnTempRegister:
				case OPrintLongLit:
				case OPrintTempReg:
					jsonGen.MakeLine("firstOperand", "{");
					jsonGen.MakeVariable("type", JSONTACOperandTypeToString(intermediateCode.code[i].firstOperand.type), JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("data", std::to_string((long long)intermediateCode.code[i].firstOperand.data), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				case OCreateTempRegInitByLongLit:
				case OPlusEqualTempRegByLongLit:
				case OMinusEqualTempRegByLongLit:
				case OTimesEqualTempRegByLongLit:
				case ODivideEqualTempRegByLongLit:
				case OModEqualTempRegByLongLit:
				case OPlusEqualTempRegByTempReg:
				case OMinusEqualTempRegByTempReg:
				case OTimesEqualTempRegByTempReg:
				case ODivideEqualTempRegByTempReg:
				case OModEqualTempRegByTempReg:
					jsonGen.MakeLine("resultOperand", "{");
					jsonGen.MakeVariable("type", JSONTACOperandTypeToString(intermediateCode.code[i].resultOperand.type), JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("data", std::to_string((long long)intermediateCode.code[i].resultOperand.data), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", true);
					jsonGen.MakeLine("firstOperand", "{");
					jsonGen.MakeVariable("type", JSONTACOperandTypeToString(intermediateCode.code[i].firstOperand.type), JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("data", std::to_string((long long)intermediateCode.code[i].firstOperand.data), JSONGenerating::JSONVarType::JVTNumber, false);
					jsonGen.MakeClose("}", false);
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(intermediateCode.code[i].op), "Operation", (void*)intermediateCode.code[i].op);
				}

				jsonGen.MakeClose("}", (i != intermediateCode.code.size() - 1) ? true : false);
			}

			jsonGen.MakeClose("]", false);
			jsonGen.MakeClose("}" ,false);
			jsonGen.MakeClose("}", false);

			return jsonGen.JSON;
		}

	private:
		std::string JSONOperationToString(Operation op) {
			switch (op) {
			case ONOP:
				return "ONOP";
			case OReturnValue:
				return "OReturnValue";
			case OReturnTempRegister:
				return "OReturnTempRegister";
			case OCreateTempRegInitByLongLit:
				return "OCreateTempRegInitByLongLit";
			case OPrintLongLit:
				return "OPrintLongLit";
			case OPrintTempReg:
				return "OPrintTempReg";
			case OPlusEqualTempRegByLongLit:
				return "OPlusEqualTempRegByLongLit";
			case OMinusEqualTempRegByLongLit:
				return "OMinusEqualTempRegByLongLit";
			case OTimesEqualTempRegByLongLit:
				return "OTimesEqualTempRegByLongLit";
			case ODivideEqualTempRegByLongLit:
				return "ODivideEqualTempRegByLongLit";
			case OModEqualTempRegByLongLit:
				return "OModEqualTempRegByLongLit";
			case OPlusEqualTempRegByTempReg:
				return "OPlusEqualTempRegByTempReg";
			case OMinusEqualTempRegByTempReg:
				return "OMinusEqualTempRegByTempReg";
			case OTimesEqualTempRegByTempReg:
				return "OTimesEqualTempRegByTempReg";
			case ODivideEqualTempRegByTempReg:
				return "ODivideEqualTempRegByTempReg";
			case OModEqualTempRegByTempReg:
				return "OModEqualTempRegByTempReg";
			default:
				err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(op), "null", 0);
			};
		}

		std::string JSONTACOperandTypeToString(TACOperandType type){
			switch (type) {
			case TACOTLong:
				return "TACOTLong";
			case TACOTTempRegister:
				return "TACOTTempRegister";
			default:
				err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(type), "null", 0);
			};
		}
	};
};
