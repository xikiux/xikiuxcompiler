#pragma once

#include <iostream>
#include <vector>

#include "Serializers/JSONGenerator.cpp"

#include "Parser.cpp"

namespace Abstracting {
	enum StatementType {
		ASTReturnNum,
		ASTReturnVar,

		ASTDeclareVarInitByLongLit,
		
		ASTPrintANum,
		ASTPrintAVar,
		
		ASTPlusEqualsANumber,
		ASTMinusEqualsANumber,
		ASTTimesEqualsANumber,
		ASTDivideEqualsANumber,
		ASTModEqualsANumber,

		ASTPlusEqualsAVariable,
		ASTMinusEqualsAVariable,
		ASTTimesEqualsAVariable,
		ASTDivideEqualsAVariable,
		ASTModEqualsAVariable,
	};

	typedef struct {
		StatementType type;
		std::string resultVar;
		long long firstOperandNum;
		std::string firstOperandVar;
	} Statement;

	typedef struct {
		std::string returnType;
		std::string name;
		std::vector<Statement> statements;
	} Function;

	typedef struct {
		Function function;
	} AST;

	class Abstracter {
		ErrorHandling::ErrorHandler* err;
		SymbolTracking::SymbolTable* sym;
	public:
		AST ast;

		Abstracter(ErrorHandling::ErrorHandler* errorHandler, SymbolTracking::SymbolTable* symbolTable) {
			err = errorHandler;
			sym = symbolTable;
		}

		void Abstract(Parsing::Program* syntaxTree) {
			ast.function = ConvertFunction(syntaxTree->function);
		}
	private:

		Function ConvertFunction(Parsing::Function parseFunc){
			Function function = Function();

			function.name = parseFunc.functionDec.functionName;
			function.returnType = parseFunc.functionDec.returnType;
			function.statements = GetStatements(parseFunc.functionBody);

			return function;
		}

		std::vector<Statement> GetStatements(Parsing::FunctionBody funcBody) {
			std::vector<Statement> statements = std::vector<Statement>();

			Parsing::Statements* tempStatements = &funcBody.statements;

			while (tempStatements != 0)
			{
				statements.push_back(GetStatement(tempStatements));
				tempStatements = (Parsing::Statements*)tempStatements->nextStatements;
			}
			
			return statements;
		}

		Statement GetStatement(Parsing::Statements* statementsObj){
			Statement statement = Statement();

			switch (statementsObj->statement.type){
			case Parsing::StatementType::STReturnNumStatement:
				statement.type = ASTReturnNum;
				statement.firstOperandNum = statementsObj->statement.returnANumber.returnValue;
				return statement;
			case Parsing::StatementType::STReturnVarStatement:
				if (sym->ContainsSymbol(statementsObj->statement.returnAVariable.identifier)) {
					statement.type = ASTReturnVar;
					statement.resultVar = statementsObj->statement.returnAVariable.identifier;
					return statement;
				} else {
					err->ThrowError(ErrorHandling::ErrorType::ETIdentifierUsedButNotDeclared, "Identifier " + statementsObj->statement.returnAVariable.identifier + " is used but never declared.", "null", 0);
				}
			case Parsing::StatementType::STDeclareVarInitByLongLitStatement:
				if (!sym->ContainsSymbol(statementsObj->statement.declareAVarWithLongLit.identifier)) { // check for identifier already existing
					sym->AddVariable(statementsObj->statement.declareAVarWithLongLit.identifier);

					statement.type = ASTDeclareVarInitByLongLit;
					statement.resultVar = statementsObj->statement.declareAVarWithLongLit.identifier;
					statement.firstOperandNum = statementsObj->statement.declareAVarWithLongLit.numValue;
					return statement;
				} else {
					err->ThrowError(ErrorHandling::ErrorType::ETIdentifierAlreadyDeclared, "Identifier " + statementsObj->statement.declareAVarWithLongLit.identifier + " is already declared.", "null", (void*)0);
				}
			case Parsing::StatementType::STPrintANumber:
				statement.type = ASTPrintANum;
				statement.firstOperandNum = statementsObj->statement.printANumber.numValue;
				return statement;
			case Parsing::StatementType::STPrintAVariable:
				statement.type = ASTPrintAVar;
				statement.resultVar = statementsObj->statement.printAVariable.identifier;
				return statement;
			case Parsing::StatementType::STPlusEqualsANumber:
				statement.type = ASTPlusEqualsANumber;
				statement.resultVar = statementsObj->statement.plusEqualsNum.identifier;
				statement.firstOperandNum = statementsObj->statement.plusEqualsNum.numValue;
				return statement;
			case Parsing::StatementType::STMinusEqualsANumber:
				statement.type = ASTMinusEqualsANumber;
				statement.resultVar = statementsObj->statement.minusEqualsNum.identifier;
				statement.firstOperandNum = statementsObj->statement.minusEqualsNum.numValue;
				return statement;
			case Parsing::StatementType::STTimesEqualsANumber:
				statement.type = ASTTimesEqualsANumber;
				statement.resultVar = statementsObj->statement.timesEqualsNum.identifier;
				statement.firstOperandNum = statementsObj->statement.timesEqualsNum.numValue;
				return statement;
			case Parsing::StatementType::STDivideEqualsANumber:
				statement.type = ASTDivideEqualsANumber;
				statement.resultVar = statementsObj->statement.divideEqualsNum.identifier;
				statement.firstOperandNum = statementsObj->statement.divideEqualsNum.numValue;
				return statement;
			case Parsing::StatementType::STModEqualsANumber:
				statement.type = ASTModEqualsANumber;
				statement.resultVar = statementsObj->statement.modEqualsNum.identifier;
				statement.firstOperandNum = statementsObj->statement.modEqualsNum.numValue;
				return statement;
			case Parsing::StatementType::STPlusEqualsAVariable:
				statement.type = ASTPlusEqualsAVariable;
				statement.resultVar = statementsObj->statement.plusEqualsVar.resultIdentifier;
				statement.firstOperandVar = statementsObj->statement.plusEqualsVar.operandIdentifier;
				return statement;
			case Parsing::StatementType::STMinusEqualsAVariable:
				statement.type = ASTMinusEqualsAVariable;
				statement.resultVar = statementsObj->statement.minusEqualsVar.resultIdentifier;
				statement.firstOperandVar = statementsObj->statement.minusEqualsVar.operandIdentifier;
				return statement;
			case Parsing::StatementType::STTimesEqualsAVariable:
				statement.type = ASTTimesEqualsAVariable;
				statement.resultVar = statementsObj->statement.timesEqualsVar.resultIdentifier;
				statement.firstOperandVar = statementsObj->statement.timesEqualsVar.operandIdentifier;
				return statement;
			case Parsing::StatementType::STDivideEqualsAVariable:
				statement.type = ASTDivideEqualsAVariable;
				statement.resultVar = statementsObj->statement.divideEqualsVar.resultIdentifier;
				statement.firstOperandVar = statementsObj->statement.divideEqualsVar.operandIdentifier;
				return statement;
			case Parsing::StatementType::STModEqualsAVariable:
				statement.type = ASTModEqualsAVariable;
				statement.resultVar = statementsObj->statement.modEqualsVar.resultIdentifier;
				statement.firstOperandVar = statementsObj->statement.modEqualsVar.operandIdentifier;
				return statement;
			default:
				err->ThrowError(ErrorHandling::ErrorType::ETAbstractingIncomplete, "Could not determine what kind of statement was given.", "Parsing::Statements*", (void*)statementsObj);
			}
		}
	public:
		std::string GenerateJSON() {
			JSONGenerating::JSONGenerator jsonGen = JSONGenerating::JSONGenerator();

			// generate json line by line
			jsonGen.MakeOpen("{");
			jsonGen.MakeLine("ast", "{");
			jsonGen.MakeLine("function", "{");
			jsonGen.MakeVariable("returnType", ast.function.returnType, JSONGenerating::JSONVarType::JVTString, true);
			jsonGen.MakeVariable("name", ast.function.name, JSONGenerating::JSONVarType::JVTString, true);
			jsonGen.MakeLine("statements", "[");
			
			for (int i = 0; i < ast.function.statements.size(); i++) {
				jsonGen.MakeLine("statement", "{");
				jsonGen.MakeVariable("type", JSONTypeIntToString(ast.function.statements[i].type), JSONGenerating::JSONVarType::JVTString, true);
				switch (ast.function.statements[i].type) {
				case ASTReturnNum:
					jsonGen.MakeVariable("firstOperandNum", std::to_string(ast.function.statements[i].firstOperandNum), JSONGenerating::JSONVarType::JVTNumber, false);
					break;
				case ASTReturnVar:
					jsonGen.MakeVariable("resultVar", ast.function.statements[i].resultVar, JSONGenerating::JSONVarType::JVTString, false);
					break;
				case ASTDeclareVarInitByLongLit:
					jsonGen.MakeVariable("resultVar", ast.function.statements[i].resultVar, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("firstOperandNum", std::to_string(ast.function.statements[i].firstOperandNum), JSONGenerating::JSONVarType::JVTNumber, false);
					break;
				case ASTPrintANum:
					jsonGen.MakeVariable("firstOperandNum", std::to_string(ast.function.statements[i].firstOperandNum), JSONGenerating::JSONVarType::JVTNumber, false);
					break;
				case ASTPrintAVar:
					jsonGen.MakeVariable("resultVar", ast.function.statements[i].resultVar, JSONGenerating::JSONVarType::JVTString, false);
					break;
				case ASTPlusEqualsANumber:
				case ASTMinusEqualsANumber:
				case ASTTimesEqualsANumber:
				case ASTDivideEqualsANumber:
				case ASTModEqualsANumber:
					jsonGen.MakeVariable("resultVar", ast.function.statements[i].resultVar, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("firstOperandNum", std::to_string(ast.function.statements[i].firstOperandNum), JSONGenerating::JSONVarType::JVTNumber, false);
					break;
				case ASTPlusEqualsAVariable:
				case ASTMinusEqualsAVariable:
				case ASTTimesEqualsAVariable:
				case ASTDivideEqualsAVariable:
				case ASTModEqualsAVariable:
					jsonGen.MakeVariable("resultVar", ast.function.statements[i].resultVar, JSONGenerating::JSONVarType::JVTString, true);
					jsonGen.MakeVariable("firstOperandVar", ast.function.statements[i].firstOperandVar, JSONGenerating::JSONVarType::JVTString, true);
					break;
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(ast.function.statements[i].type), "StatementType", (void*)ast.function.statements[i].type);
				}

				jsonGen.MakeClose("}", (i != ast.function.statements.size() - 1) ? true : false);
			}

			jsonGen.MakeClose("]", false);
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);
			jsonGen.MakeClose("}", false);

			return jsonGen.JSON;
		}
	private:

		std::string JSONTypeIntToString(StatementType type) {
			switch (type) {
				case ASTReturnNum:
					return "ASTReturnNum";
				case ASTReturnVar:
					return "ASTReturnVar";
				case ASTDeclareVarInitByLongLit:
					return "ASTDeclareVarInitByLongLit";
				case ASTPrintANum:
					return "ASTPrintANum";
				case ASTPrintAVar:
					return "ASTPrintAVar";
				case ASTPlusEqualsANumber:
					return "ASTPlusEqualsANumber";
				case ASTMinusEqualsANumber:
					return "ASTMinusEqualsANumber";
				case ASTTimesEqualsANumber:
					return "ASTTimesEqualsANumber";
				case ASTDivideEqualsANumber:
					return "ASTDivideEqualsANumber";
				case ASTModEqualsANumber:
					return "ASTModEqualsANumber";
				case ASTPlusEqualsAVariable:
					return "ASTPlusEqualsAVariable";
				case ASTMinusEqualsAVariable:
					return "ASTMinusEqualsAVariable";
				case ASTTimesEqualsAVariable:
					return "ASTTimesEqualsAVariable";
				case ASTDivideEqualsAVariable:
					return "ASTDivideEqualsAVariable";
				case ASTModEqualsAVariable:
					return "ASTModEqualsAVariable";
				default:
					err->ThrowError(ErrorHandling::ErrorType::ETJsonToStringFailure, "In function " + std::string(__func__) + ", no json conversion was detected for " + std::to_string(type), "null", 0);
			}
		}
	};
}
