#include <iostream>
#include <string>

#include <fstream>

#include "Error/ErrorHandler.cpp"
#include "SymbolTracker.cpp"
#include "NecessaryLibraryDetector.cpp"

#include "Lib/FileIO.cpp"

#include "AssemblyWriter.cpp"
#include "IntermediateCoder.cpp"
#include "Abstracter.cpp"
#include "Parser.cpp"
#include "Lexer.cpp"

int main(int argc, char* argv[]){
	ErrorHandling::ErrorHandler errorHandler = ErrorHandling::ErrorHandler();
	SymbolTracking::SymbolTable symbolTable = SymbolTracking::SymbolTable();
	LibraryDetecting::NeededLibraries neededLibs = LibraryDetecting::NeededLibraries();
	std::string program;
	std::string ProgramName;

	// Adjust settings
	// argv[0] is ./Compiler
	// argv[1] is always the file path
	// argv[2] is always the output file's name, or left blank to be named 'output'
	if (argc > 1) {
		program = ReadFileIntoString(&errorHandler, argv[1]);

		if (argc > 2) {
			ProgramName = argv[2];
		} else {
			ProgramName = "output";
		}
	} else {
		errorHandler.ThrowHelpScreen();
		return -3;
	}

	// Compile
	// log program to console
	std::cout << "File: " << std::endl << program << std::endl << std::endl;

	// start lexing
	Lexing::Lexer lex = Lexing::Lexer(&errorHandler, &symbolTable);
	lex.Lex(&program);	
	std::cout << "Finished Lexing" << std::endl << lex.GenerateJSON() << std::endl;

	// start parsing
	Parsing::Parser parse = Parsing::Parser(&errorHandler, &symbolTable, &neededLibs);
	parse.Parse(lex.tokens);
	std::cout << "Finished Parsing" << std::endl << parse.GenerateJSON() << std::endl;

	// turn parse tree into ast
	Abstracting::Abstracter abstracter = Abstracting::Abstracter(&errorHandler, &symbolTable);
	abstracter.Abstract(&parse.program);
	std::cout << "Finished Abstracting" << std::endl << abstracter.GenerateJSON() << std::endl;

	// turn ast into three address code
	IntermediateCoding::IntermediateCoder ic = IntermediateCoding::IntermediateCoder(&errorHandler, &symbolTable);
	ic.Intermediate(&abstracter.ast);
	std::cout << "Finished Intermediating" << std::endl << ic.GenerateJSON() << std::endl;

	// turn 3 address code into assembly
	AssemblyWriting::AssemblyWriter assemblyWriter = AssemblyWriting::AssemblyWriter(&errorHandler, &symbolTable, &neededLibs);
	assemblyWriter.WriteAssemblyCode(&ic.intermediateCode);
	std::cout << "Generated Assembly: " << std::endl << assemblyWriter.assemblyCode << std::endl;

	// turn assembly into executable
	bool assemblyWritten = WriteStringToFile(ProgramName + ".s", assemblyWriter.assemblyCode);
	if (!assemblyWritten) return 6;
	system(std::string("nasm -f elf64 -o obj.o " + ProgramName + ".s && ld -o " + ProgramName + " obj.o && rm obj.o").c_str());
	std::cout << "Compilation & Linking Finished" << std::endl;

	// clean up

	return 0;
}
